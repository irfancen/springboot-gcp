package com.example.demo.todo.controller;

import com.example.demo.todo.model.Todo;
import com.example.demo.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path={"/",""})
public class TodoController {

    final TodoService todoService;

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping(path = {"/",""})
    public String home(Model model) {
        model.addAttribute("todos",todoService.getAllTodo());
        return "home";
    }

    @GetMapping(path={"/add_todo","/add_todo/{id}"})
    public String addTodo(Model model, @PathVariable(required = false) Long id) {
        if (id != null) {
            model.addAttribute("initial_value", todoService.getTodo(id));
        }
        return "todo_form";
    }

    @PostMapping(path = "/add_todo")
    public String createTodo(
            @RequestParam(value = "id", required = false) Long id,
            @RequestParam(value = "title", required = true) String title,
            @RequestParam(value = "description", required = true) String description ) {
        Todo todo = new Todo();
        todo.setTitle(title);
        todo.setDescription(description);
        if (id != -1) todo.setId(id);
        todoService.createTodo(todo);
        return "redirect:/";
    }

    @PostMapping(path="delete_todo/{id}")
    public String deleteTodo(@PathVariable Long id) {
        todoService.deleteTodo(id);
        return "redirect:/";
    }


}
