package com.example.demo.todo.service;

import com.example.demo.todo.model.Todo;

import java.util.List;

public interface TodoService {
    void createTodo(Todo todo);
    void deleteTodo(Long id);
    Todo getTodo(Long id);
    List<Todo> getAllTodo();
}
